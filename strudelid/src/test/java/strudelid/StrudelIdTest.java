/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Iterator;
import java.util.stream.Stream;

import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;
import strudelid.time.ControllableClock;
import strudelid.time.TimestampSequence;

import org.junit.Before;
import org.junit.Test;

public class StrudelIdTest {

    private final int PARTITION = 14;
    private final long TS = 1570555498; // 2019-Oct-08 5:24:58 PM
    private final long SEQ = 193;
    private final int TIME_RESOLUTION = 3;

    private ControllableClock clock;
    private TimestampSequence tssequence;
    private LayoutView layout;

    @Before
    public void setup() {
        clock = new ControllableClock(TS);
        tssequence = new TimestampSequence(TS, SEQ);
        layout = LayoutBakery.bakeDefault();
    }

    @Test
    public void testConstructor() {
        final StrudelId subject = new StrudelId(clock, 3, PARTITION, tssequence);
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(PARTITION, subject.getPartition());
        assertEquals(tssequence, subject.getTimestampSequence());
    }

    @Test
    public void testGetSeed() {
        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        assertEquals(PARTITION, seed.getPartition());
        assertEquals(TS, seed.getTimestampSequence().getTimestamp());
        assertEquals(0, seed.getTimestampSequence().getSequence());
    }

    @Test
    public void testGetNext() {
        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        Stream<StrudelId> stream = Stream.iterate(seed, StrudelId::next);
        Iterator<StrudelId> iter = stream.iterator();

        StrudelId subject;

        subject = iter.next();
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(0, subject.getTimestampSequence().getSequence());
        assertEquals(TS, subject.getTimestampSequence().getTimestamp());

        subject = iter.next();
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(1, subject.getTimestampSequence().getSequence());
        assertEquals(TS, subject.getTimestampSequence().getTimestamp());

        subject = iter.next();
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(2, subject.getTimestampSequence().getSequence());
        assertEquals(TS, subject.getTimestampSequence().getTimestamp());

        clock.advanceBy(3);

        subject = iter.next();
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(0, subject.getTimestampSequence().getSequence());
        assertEquals(TS + 3, subject.getTimestampSequence().getTimestamp());

        subject = iter.next();
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(1, subject.getTimestampSequence().getSequence());
        assertEquals(TS + 3, subject.getTimestampSequence().getTimestamp());

        subject = iter.next();
        assertEquals(clock, subject.getClock());
        assertEquals(TIME_RESOLUTION, subject.getTimeResolution());
        assertEquals(2, subject.getTimestampSequence().getSequence());
        assertEquals(TS + 3, subject.getTimestampSequence().getTimestamp());
    }

    @Test
    public void testToString() {
        final String TO_STRING = "StrudelId(clock=ControllableClock,partition=14,tssequence=TimestampSequence(1570555498.193)";
        final StrudelId subject = new StrudelId(clock, 3, PARTITION, tssequence);
        assertEquals(TO_STRING, subject.toString());
    }
}
