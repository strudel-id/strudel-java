/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.time;

import static org.junit.Assert.assertEquals;

import strudelid.time.TimestampComponents;

import org.junit.Test;

public class TimestampComponentsTest {

    final int TS_1 = 1570555498; // Tuesday, October 8, 2019 5:24:58 PM

    @Test
    public void testGetTimestamp() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final long actual = subject.getTimestamp();
        assertEquals(TS_1, actual);
    }

    @Test
    public void testGetYear() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final int actual = subject.getYear();
        assertEquals(2019, actual);
    }

    @Test
    public void testGetMonth() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final int actual = subject.getMonth();
        assertEquals(10, actual);
    }

    @Test
    public void testGetDay() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final int actual = subject.getDay();
        assertEquals(8, actual);
    }

    @Test
    public void testGetHour() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final int actual = subject.getHour();
        assertEquals(17, actual);
    }

    @Test
    public void testGetMinute() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final int actual = subject.getMinute();
        assertEquals(24, actual);
    }

    @Test
    public void testGetSecond() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final int actual = subject.getSecond();
        assertEquals(58, actual);
    }

    @Test
    public void testToString() {
        final TimestampComponents subject = new TimestampComponents(TS_1);
        final String actual = subject.toString();
        assertEquals("2019-10-08T17:24:58-00:00", actual);
    }
}