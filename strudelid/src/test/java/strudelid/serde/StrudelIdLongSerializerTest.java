/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import strudelid.StrudelDatum;
import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;
import strudelid.time.TimestampSequence;

import org.junit.Before;
import org.junit.Test;

public class StrudelIdLongSerializerTest {

    private final int PARTITION = 14;
    private final long TS = 1570555498; // 2019-Oct-08 5:24:58 PM
    private final long SEQ = 193;
    private final long BITS_TS_SEQ = 6646576849219L;

    private TimestampSequence tssequence;
    private LayoutView layout;
    private FormatView format;

    @Before
    public void setup() {
        tssequence = new TimestampSequence(TS, SEQ);
        layout = LayoutBakery.bakeDefault();
        format = FormatBakery.bakeDefault();
    }

    @Test
    public void testSerialize() throws IOException {

        final StrudelDatum datum = new StrudelDatum(PARTITION, tssequence);

        final StrudelIdLongSerializer subject = new StrudelIdLongSerializer();
        subject.configure(layout, format);

        long actual = subject.serialize(datum);

        assertEquals(BITS_TS_SEQ, actual);

        subject.close();
    }

    @Test
    public void testPackSequence() {
        assertEquals(0, packSequence5(0 * 32 + 0));
        assertEquals(1, packSequence5(0 * 32 + 1));
        assertEquals(15, packSequence5(0 * 32 + 15));
        assertEquals(16, packSequence5(0 * 32 + 16));
        assertEquals(31, packSequence5(0 * 32 + 31));

        assertEquals(32, packSequence5(1 * 32 + 0));
        assertEquals(33, packSequence5(1 * 32 + 1));
        assertEquals(47, packSequence5(1 * 32 + 15));
        assertEquals(48, packSequence5(1 * 32 + 16));
        assertEquals(63, packSequence5(1 * 32 + 31));

        assertEquals(992, packSequence5(31 * 32 + 0));
        assertEquals(993, packSequence5(31 * 32 + 1));
        assertEquals(1007, packSequence5(31 * 32 + 15));
        assertEquals(1008, packSequence5(31 * 32 + 16));
        assertEquals(1023, packSequence5(31 * 32 + 31));

        assertEquals(1024, packSequence5(32 * 32 + 0));
        assertEquals(1025, packSequence5(32 * 32 + 1));
        assertEquals(1039, packSequence5(32 * 32 + 15));
        assertEquals(1040, packSequence5(32 * 32 + 16));
        assertEquals(1055, packSequence5(32 * 32 + 31));
    }

    private long packSequence5(long sequence) {
        return StrudelIdLongSerializer.packSequence(0b11111, 5, sequence);
    }
}