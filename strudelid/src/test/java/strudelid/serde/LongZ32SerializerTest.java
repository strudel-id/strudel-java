/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;

import org.junit.Before;
import org.junit.Test;

public class LongZ32SerializerTest {

    private final long BITS_TS_SEQ = 1321387270467L;
    private final String ID_TS_SEQ = "DKE DPW QGB"; // Respect configuration separator!

    private LayoutView layout;
    private FormatView format;

    @Before
    public void setup() {
        layout = LayoutBakery.bakeDefault();
        format = FormatBakery.bakeDefault();
    }

    @Test
    public void testSerialize() throws IOException {

        final long datum = BITS_TS_SEQ;

        final LongZ32Serializer subject = new LongZ32Serializer();
        subject.configure(layout, format);

        String actual = subject.serialize(datum);

        assertEquals(ID_TS_SEQ, actual);

        subject.close();
    }
}