/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import strudelid.StrudelId;
import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;
import strudelid.serde.FormatBakery.Flavor;
import strudelid.time.ControllableClock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StrudelIdZ32SerializerTest {

    private final int PARTITION = 14;
    private final long TS = 1570555498; // 2019-Oct-08 5:24:58 PM
    private final long TS_1 = 1570555499; // 2019-Oct-08 5:24:59 PM
    private final long TS_3 = 1570555501; // 2019-Oct-08 5:25:01 PM
    private final String ID32_TS = "DKE WPD Q"; // Respect default format!
    private final String ID32_TS_1_1 = "DKE WPD QB";
    private final String ID32_TS_1_2 = "DKE WPD QN";
    private final String ID32_TS_3 = "DKE WPR Q";
    private final int ID_COUNT = 15000;

    private ControllableClock clock;
    private StrudelIdZ32Serializer serializer;
    private LayoutView layout;
    private FormatView format;

    @Before
    public void setup() {
        clock = new ControllableClock(TS);
        serializer = new StrudelIdZ32Serializer();
        layout = LayoutBakery.bakeDefault();
        format = FormatBakery.bakeDefault();
        serializer.configure(layout, format);
    }

    @After
    public void teardown() {
        serializer.close();
    }

    @Test
    public void testOrdering() {
        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        Stream<StrudelId> stream;
        StrudelId id;
        String actual;
        FormatView format;

        format = FormatBakery.bake(Flavor.UCASE_SPACE_EVERY3);
        serializer.configure(layout, format);

        stream = Stream.iterate(seed, StrudelId::next);
        id = stream.findFirst().get();
        actual = serializer.serialize(id);
        assertEquals("DKE WPD Q", actual);

        format = FormatBakery.bake(Flavor.REV_UCASE_SPACE_EVERY3);
        serializer.configure(layout, format);
        stream = Stream.iterate(seed, StrudelId::next);
        id = stream.findFirst().get();
        actual = serializer.serialize(id);
        assertEquals("QDP WEK D", actual);
    }

    @Test
    public void testCasing() {
        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        Stream<StrudelId> stream;
        StrudelId id;
        String actual;
        FormatView format;

        format = FormatBakery.bake(Flavor.LCASE_SPACE_EVERY3);
        serializer.configure(layout, format);

        stream = Stream.iterate(seed, StrudelId::next);
        id = stream.findFirst().get();
        actual = serializer.serialize(id);
        assertEquals("dke wpd q", actual);

        format = FormatBakery.bake(Flavor.UCASE_SPACE_EVERY3);
        serializer.configure(layout, format);
        stream = Stream.iterate(seed, StrudelId::next);
        id = stream.findFirst().get();
        actual = serializer.serialize(id);
        assertEquals("DKE WPD Q", actual);
    }

    @Test
    public void testSplitter() {
        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        Stream<StrudelId> stream;
        StrudelId id;
        String actual;
        FormatView format;

        format = FormatBakery.bake(Flavor.UCASE_SPACE_EVERY3);
        serializer.configure(layout, format);

        stream = Stream.iterate(seed, StrudelId::next);
        id = stream.findFirst().get();
        actual = serializer.serialize(id);
        assertEquals("DKE WPD Q", actual);

        format = FormatBakery.bake(Flavor.UCASE);
        serializer.configure(layout, format);
        stream = Stream.iterate(seed, StrudelId::next);
        id = stream.findFirst().get();
        actual = serializer.serialize(id);
        assertEquals("DKEWPDQ", actual);
    }

    @Test
    public void testShortTimeframe() {

        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        Stream<StrudelId> stream = Stream.iterate(seed, StrudelId::next);
        Iterator<StrudelId> iter = stream.iterator();

        StrudelId subject;
        String subject32;

        subject = iter.next();
        assertEquals(0, subject.getTimestampSequence().getSequence());
        assertEquals(TS, subject.getTimestampSequence().getTimestamp());
        subject32 = serializer.serialize(subject);
        assertEquals(ID32_TS, subject32);

        clock.advanceBy(1);

        subject = iter.next();
        assertEquals(1, subject.getTimestampSequence().getSequence());
        assertEquals(TS_1, subject.getTimestampSequence().getTimestamp());
        subject32 = serializer.serialize(subject);
        assertEquals(ID32_TS_1_1, subject32);

        subject = iter.next();
        assertEquals(2, subject.getTimestampSequence().getSequence());
        assertEquals(TS_1, subject.getTimestampSequence().getTimestamp());
        subject32 = serializer.serialize(subject);
        assertEquals(ID32_TS_1_2, subject32);

        clock.advanceBy(2);

        subject = iter.next();
        assertEquals(0, subject.getTimestampSequence().getSequence());
        assertEquals(TS_3, subject.getTimestampSequence().getTimestamp());
        subject32 = serializer.serialize(subject);
        assertEquals(ID32_TS_3, subject32);
    }

    @Test
    public void testLargeSetNoDups() {

        final StrudelId seed = StrudelId.getSeed(layout, clock, PARTITION);
        Stream<StrudelId> stream = Stream.iterate(seed, StrudelId::next);

        Set<String> allSids = new HashSet<String>();
        //@formatter:off
        Set<String> dupSids = stream
            .limit(ID_COUNT)
            .map(serializer::serialize)
            .filter(s -> !allSids.add(s))
            .collect(Collectors.toSet());
        //@formatter:on

        assertEquals(ID_COUNT, allSids.size());
        assertEquals(0, dupSids.size());
    }
}