/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import static org.junit.Assert.assertEquals;

import strudelid.StrudelDatum;
import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;
import strudelid.serde.FormatBakery.Flavor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StrudelIdZ32DeserializerTest {

    private final int PARTITION = 14;
    private final long TS_P = 1570555497; // 2019-Oct-08 5:24:57 PM
    private final long TS_P_3 = 1570555500; // 2019-Oct-08 5:24:57 PM
    private final String ID32_TS = "DKE DPW Q"; // Respect configuration separator!
    private final String ID32_TS_1_1 = "DKE DPW QB";
    private final String ID32_TS_1_2 = "DKE DPW QN";
    private final String ID32_TS_3 = "DKE RPW Q";

    private StrudelIdZ32Deserializer deserializer;

    private LayoutView layout;
    private FormatView format;

    @Before
    public void setup() {
        layout = LayoutBakery.bakeDefault();
        format = FormatBakery.bakeDefault();
        deserializer = new StrudelIdZ32Deserializer();
        deserializer.configure(layout, format, 2019);
    }

    @After
    public void teardown() {
        deserializer.close();
    }

    @Test
    public void testOrdering() {

        StrudelDatum actual;

        format = FormatBakery.bake(Flavor.UCASE_SPACE_EVERY3);
        deserializer.configure(layout, format, 2019);
        actual = deserializer.deserialize("DKE DPW Q");
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());
        assertEquals(0, actual.getTimestampSequence().getSequence());

        format = FormatBakery.bake(Flavor.REV_UCASE_SPACE_EVERY3);
        deserializer.configure(layout, format, 2019);
        actual = deserializer.deserialize("QWP DEK D");
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());
        assertEquals(0, actual.getTimestampSequence().getSequence());
    }

    @Test
    public void testCasing() {

        StrudelDatum actual;

        format = FormatBakery.bake(Flavor.LCASE_SPACE_EVERY3);
        deserializer.configure(layout, format, 2019);
        actual = deserializer.deserialize("dke dpw q");
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());
        assertEquals(0, actual.getTimestampSequence().getSequence());

        format = FormatBakery.bake(Flavor.UCASE_SPACE_EVERY3);
        deserializer.configure(layout, format, 2019);
        actual = deserializer.deserialize("DKE DPW Q");
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());
        assertEquals(0, actual.getTimestampSequence().getSequence());
    }

    @Test
    public void testShortTimeframe() {

        StrudelDatum actual;

        actual = deserializer.deserialize(ID32_TS);
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());

        actual = deserializer.deserialize(ID32_TS_1_1);
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());

        actual = deserializer.deserialize(ID32_TS_1_2);
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P, actual.getTimestampSequence().getTimestamp());

        actual = deserializer.deserialize(ID32_TS_3);
        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TS_P_3, actual.getTimestampSequence().getTimestamp());
    }
}