/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import strudelid.StrudelDatum;
import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;

import org.junit.Before;
import org.junit.Test;

public class StrudelIdLongDeserializerTest {

    private final int PARTITION = 14;
    private final long TSPART = 1570555497; // 2019-Oct-08 5:24:57 PM
    private final long BITS_TSPART_SEQ = 15717212483L;

    private LayoutView layout;
    private FormatView format;

    @Before
    public void setup() {
        layout = LayoutBakery.bakeDefault();
        format = FormatBakery.bakeDefault();
    }

    @Test
    public void testDeserialize() throws IOException {

        long bits = BITS_TSPART_SEQ;

        final StrudelIdLongDeserializer subject = new StrudelIdLongDeserializer();
        subject.configure(layout, format, 2019);

        StrudelDatum actual = subject.deserialize(bits);

        assertEquals(PARTITION, actual.getPartition());
        assertEquals(TSPART, actual.getTimestampSequence().getTimestamp());

        subject.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackSegments_NegativeSegmentCount() {
        StrudelIdLongDeserializer.unpackSegments(0b11111, 5, 1234567L, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackYear_EpochYearInDistantPast() {
        StrudelIdLongDeserializer.unpackYear(0b11111, 1, 2015);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackYear_EpochYearInDistantFuture() {
        StrudelIdLongDeserializer.unpackYear(0b11111, 1, 3000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackMonth_MonthLessThanOne() {
        StrudelIdLongDeserializer.unpackMonth(0b11111, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackMonth_MonthGreaterThanTwelve() {
        StrudelIdLongDeserializer.unpackMonth(0b11111, 13);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackDay_DayLessThanOne() {
        StrudelIdLongDeserializer.unpackDay(0b1111111111, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackDay_DayGreaterThanThirtyOne() {
        StrudelIdLongDeserializer.unpackDay(0b1111111111, 32);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackTimePeriod_TimePeriodLessThanOne() {
        StrudelIdLongDeserializer.unpackTimePeriod(0b11111, 5, 3, 20, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnpackTimePeriod_TimePeriodGreaterThanThirtyOne() {
        StrudelIdLongDeserializer.unpackTimePeriod(0b11111, 5, 3, 20, 21);
    }
}