/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.layout;

import static strudelid.layout.LayoutBakery.Flavor.APPLE;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StrudelIdLayoutTest {

    @Test
    public void testBakeApple() {
        Layout flavor = LayoutBakery.bake(APPLE);
        assertApple(flavor);
    }

    @Test
    public void testBakeDefault() {
        Layout flavor = LayoutBakery.bakeDefault();
        assertApple(flavor);
    }

    protected void assertApple(Layout flavor) {
        assertEquals(5, flavor.getBitsPerSegment());
        assertEquals(32, flavor.getMaxSegmentValue());
        assertEquals(0b11111, flavor.getSegmentMask());
        assertEquals(1, flavor.getSegmentsPerPartition());
        assertEquals(3, flavor.getSegmentsPerTime());
        assertEquals(1, flavor.getSegmentsPerYear());
        assertEquals(1, flavor.getSegmentsPerMonth());
        assertEquals(1, flavor.getSegmentsPerDay());
        assertEquals(3, flavor.getSegmentsPerDate());
        assertEquals(3, flavor.getTimeResolution());
        assertEquals(20, flavor.getTimePeriodsPerMinute());
        assertEquals(28800, flavor.getTimePeriodsPerDay());
    }
}