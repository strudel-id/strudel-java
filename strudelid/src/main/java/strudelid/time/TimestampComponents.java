/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.time;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TimestampComponents {

    final private long timestamp;
    final private int year;
    final private int month;
    final private int day;
    final private int hour;
    final private int minute;
    final private int second;

    public TimestampComponents(long timestamp) {
        LocalDateTime d = LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC);

        this.timestamp = timestamp;

        this.year = d.getYear();
        this.month = d.getMonth().getValue();
        this.day = d.getDayOfMonth();
        this.hour = d.getHour();
        this.minute = d.getMinute();
        this.second = d.getSecond();
    }

    public static TimestampComponents of(int year, int month, int day, long timePeriod) {
        int f = (int) ((timePeriod / 28800.0) * 86400.0);
        int hour = f / (60 * 60);
        f -= hour * 60 * 60;
        int minute = f / 60;
        int second = f - minute * 60;

        return new TimestampComponents(year, month, day, hour, minute, second);
    }

    public TimestampComponents(int year, int month, int day, int hour, int minute, int second) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.timestamp = LocalDateTime.of(year, month, day, hour, minute, second).toEpochSecond(ZoneOffset.UTC);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public String toString() {
        return String.format("%04d-%02d-%02dT%02d:%02d:%02d-00:00", year, month, day, hour, minute, second);
    }
}