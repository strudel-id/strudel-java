/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import strudelid.StrudelDatum;
import strudelid.layout.LayoutView;
import strudelid.time.TimestampComponents;
import strudelid.time.TimestampSequence;

public class StrudelIdLongDeserializer implements Deserializer<StrudelDatum, Long> {

    private long segmentMask;
    private int bitsPerSegment;
    private int segmentsPerYear;
    private int segmentsPerMonth;
    private int segmentsPerDay;
    private int segmentsPerTime;
    private int timePeriodsPerDay;
    private int epochYear;

    @Override
    public void close() {
    }

    @Override
    public void configure(LayoutView layout, FormatView format, int asOfYear) {
        segmentMask = layout.getSegmentMask();
        bitsPerSegment = layout.getBitsPerSegment();
        segmentsPerYear = layout.getSegmentsPerYear();
        segmentsPerMonth = layout.getSegmentsPerMonth();
        segmentsPerDay = layout.getSegmentsPerDay();
        segmentsPerTime = layout.getSegmentsPerTime();
        timePeriodsPerDay = layout.getTimePeriodsPerDay();
        epochYear = asOfYear - (asOfYear % (segmentsPerYear * layout.getMaxSegmentValue()));
    }

    @Override
    public StrudelDatum deserialize(Long bits) {
        int year = unpackYear(segmentMask, bits, epochYear);
        bits >>= segmentsPerYear * bitsPerSegment;
        int month = unpackMonth(segmentMask, bits);
        bits >>= segmentsPerMonth * bitsPerSegment;
        int day = unpackDay(segmentMask, bits);
        bits >>= segmentsPerDay * bitsPerSegment;
        long timePeriod = unpackTimePeriod(segmentMask, bitsPerSegment, segmentsPerTime, timePeriodsPerDay, bits);
        bits >>= segmentsPerTime * bitsPerSegment;
        TimestampComponents tscomp = TimestampComponents.of(year, month, day, timePeriod);
        int partition = unpackPartition(segmentMask, bits);
        bits >>= bitsPerSegment;
        long sequence = unpackSequence(segmentMask, bitsPerSegment, bits);

        TimestampSequence tssequence = new TimestampSequence(tscomp.getTimestamp(), sequence);

        return new StrudelDatum(partition, tssequence);
    }

    private static byte unpackSegment(long segmentMask, long bits) {
        return (byte) (bits & segmentMask);
    }

    public static long unpackSegments(long segmentMask, int bitsPerSegment, long bits, int n) {
        if (n < 0) {
            throw new IllegalArgumentException("segment count must be non-negative!");
        }

        final byte[] part = new byte[n];

        for (int i = 0; i < n; i++, bits >>= bitsPerSegment) {
            part[i] = (byte) (bits & segmentMask);
        }

        long result = 0;
        for (int i = n - 1; i >= 0; i--) {
            result <<= bitsPerSegment;
            result |= part[i];
        }

        return result;
    }

    public static int unpackYear(long segmentMask, long bits, int epochYear) {
        if (epochYear < 2016 || epochYear > 2999) {
            throw new IllegalArgumentException("epoch year must be in range [2016..2999]");
        }
        int year = inferYear(unpackSegment(segmentMask, bits), epochYear);

        return year;
    }

    public static int unpackMonth(long segmentMask, long bits) {
        byte month = unpackSegment(segmentMask, bits);
        if (month < 1 || month > 12) {
            throw new IllegalArgumentException("segment does not represent a valid month [1..12]");
        }

        return month;
    }

    public static int unpackDay(long segmentMask, long bits) {
        byte day = unpackSegment(segmentMask, bits);
        if (day < 1 || day > 31) {
            throw new IllegalArgumentException("segment does not represent a valid day [1..31]");
        }
        return day;
    }

    public static long unpackTimePeriod(long segmentMask, int bitsPerSegment, int segmentsPerTime,
            int timePeriodsPerDay, long bits) {
        long timePeriod = unpackSegments(segmentMask, bitsPerSegment, bits, segmentsPerTime);
        if (timePeriod < 1 || timePeriod > timePeriodsPerDay) {
            throw new IllegalArgumentException("segment does not represent a valid day [1..31]");
        }
        return timePeriod;
    }

    private static int unpackPartition(long segmentMask, long bits) {
        byte partition = unpackSegment(segmentMask, bits);

        return partition;
    }

    private static long unpackSequence(long segmentMask, int bitsPerSegment, long bits) {
        long sequence = 0;
        while (bits != 0) {
            sequence <<= bitsPerSegment;
            sequence |= bits & segmentMask;
            bits >>= bitsPerSegment;
        }
        return sequence;
    }

    private static int inferYear(int modulo, int epochYear) {
        return epochYear + modulo;
    }

}