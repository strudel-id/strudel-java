/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import strudelid.layout.LayoutView;

public class LongDebugSerializer implements Serializer<Long, String> {

    //@formatter:off
    private static final String[] LABELS = new String[] {
        "Q4", "Q3", "Q2", "Q1", "Q0",
        "P",
        "T2", "T1", "T0",
        "D", "M", "Y"
    };
    //@formatter:on
    private static final String EVERY_N_CHARS = "(?<=\\G.....)";
    private static final String DEF_SEPARATOR = "┊";

    private String separator = DEF_SEPARATOR;

    @Override
    public void close() {
    }

    @Override
    public void configure(LayoutView layout, FormatView format) {
    }

    @Override
    public String serialize(Long datum) {

        StringBuilder sb = new StringBuilder();

        final String sbits = String.format("%1$60s", Long.toBinaryString(datum)).replace(' ', '0');
        final String[] binaryParts = sbits.split(EVERY_N_CHARS);

        sb.append(separator);
        sb.append(String.join(separator, binaryParts) + separator);
        sb.append("\n");
        sb.append(separator);
        for (int i = 0; i < binaryParts.length; i++) {
            sb.append(String.format("%4d %s", Integer.parseInt(binaryParts[i], 2), separator));
        }
        sb.append("\n");
        sb.append(separator);
        for (int i = 0; i < LABELS.length; i++) {
            sb.append(String.format("%4s %s", LABELS[i], separator));
        }
        sb.append("\n");

        return sb.toString();
    }
}
