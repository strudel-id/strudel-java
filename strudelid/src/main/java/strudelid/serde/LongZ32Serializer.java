/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import strudelid.layout.LayoutView;

public class LongZ32Serializer implements Serializer<Long, String>, ZBase32 {

    // Lambda types
    //@formatter:off
    private static final Function<StringBuilder, StringBuilder>
        withOriginal = (StringBuilder orig) -> orig;
    private static final Function<StringBuilder, StringBuilder>
        withReverseOrder = (StringBuilder orig) -> orig .reverse();
    private static final Function<StringBuilder, StringBuilder>
        withUpperCasing = (StringBuilder orig) -> new StringBuilder(orig.toString().toUpperCase());
    //@formatter:on

    private int bitsPerSegment;
    private long mask;
    private String separator;
    private IntUnaryOperator splitter;
    private Function<StringBuilder, StringBuilder> ordering;
    private Function<StringBuilder, StringBuilder> casing;
    private Function<StringBuilder, String> withSeparator;

    @Override
    public void close() {
    }

    @Override
    public void configure(LayoutView layout, FormatView format) {
        bitsPerSegment = layout.getBitsPerSegment();
        mask = layout.getSegmentMask();
        ordering = format.isReverse() ? withReverseOrder : withOriginal;
        casing = format.isUpperCase() ? withUpperCasing : withOriginal;
        separator = format.getSeparator();
        splitter = getSplitter(format.getSplitter());
        withSeparator = (StringBuilder orig) -> {
            AtomicInteger counter = new AtomicInteger(0);
            //@formatter:off
            final Function<String,Integer> every = (s) -> splitter.applyAsInt(counter.getAndIncrement());
            String result = orig
                    .chars()
                    .mapToObj(c -> String.valueOf((char) c))
                    .collect(Collectors.groupingBy(every, Collectors.joining("")))
                    .values().stream()
                    .collect(Collectors.joining(separator));
            //@formatter:on

            return result;
        };
    }

    @Override
    public String serialize(Long datum) {

        StringBuilder sb = new StringBuilder();
        while (datum != 0) {
            sb.append(ZBase32.ALPHABET[(int) (datum & mask)]);
            datum >>= bitsPerSegment;
        }

        //@formatter:off
        return Stream.of(sb)
            .map(ordering)
            .map(casing)
            .map(withSeparator)
            .collect(Collectors.joining());
        //@formatter:on
    }

    private static IntUnaryOperator getSplitter(String fnkey) {
        IntUnaryOperator fn;
        switch (fnkey) {
            case "EVERY3":
                fn = (n) -> n / 3;
                break;
            default:
                fn = (n) -> 0;
        }
        return fn;
    }
}