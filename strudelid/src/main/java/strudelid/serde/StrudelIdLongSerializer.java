/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import strudelid.StrudelDatum;
import strudelid.layout.LayoutView;
import strudelid.time.TimestampComponents;
import strudelid.time.TimestampSequence;

public class StrudelIdLongSerializer implements Serializer<StrudelDatum, Long> {

    private int bitsPerSegment;
    private long segmentMask;
    private int segmentsPerPartition;
    private int segmentsPerDate;
    private int segmentsPerTime;
    private int timeResolution;

    @Override
    public void close() {
    }

    @Override
    public void configure(LayoutView layout, FormatView format) {
        bitsPerSegment = layout.getBitsPerSegment();
        segmentMask = layout.getSegmentMask();
        segmentsPerPartition = layout.getSegmentsPerPartition();
        segmentsPerDate = layout.getSegmentsPerDate();
        segmentsPerTime = layout.getSegmentsPerTime();
        timeResolution = layout.getTimeResolution();
    }

    @Override
    public Long serialize(StrudelDatum datum) {
        final TimestampSequence tssequence = datum.getTimestampSequence();
        final TimestampComponents tscomp = new TimestampComponents(tssequence.getTimestamp());

        long result = 0;

        result |= packSequence(segmentMask, bitsPerSegment, tssequence.getSequence());
        result <<= segmentsPerPartition * bitsPerSegment;
        result |= packPartition(segmentMask, bitsPerSegment, datum.getPartition());
        result <<= segmentsPerTime * bitsPerSegment;
        result |= packTime(segmentMask, bitsPerSegment, segmentsPerTime, timeResolution, tscomp);
        result <<= segmentsPerDate * bitsPerSegment;
        result |= packDate(segmentMask, bitsPerSegment, tscomp);

        return result;
    }

    public static long packSequence(long segmentMask, int bitsPerSegment, long sequence) {
        long result = 0;

        int lastSegBitIndex = Long.SIZE - (Long.SIZE % bitsPerSegment);
        for (int i = lastSegBitIndex; i >= 0; i -= bitsPerSegment) {
            result <<= bitsPerSegment;
            result |= (sequence >> i) & segmentMask;
        }

        return result;
    }

    public static long packPartition(long segmentMask, int bitsPerSegment, int partition) {
        return partition & segmentMask;
    }

    private static long packTime(long segmentMask, int bitsPerSegment, int segmentsPerTime, int timeResolution,
            TimestampComponents tscomp) {

        int timePeriodsPerMinute = 60 / timeResolution;

        int tpart = timePeriodsPerMinute * 60 * tscomp.getHour() + timePeriodsPerMinute * tscomp.getMinute()
                + tscomp.getSecond() / timeResolution;

        byte[] part = new byte[segmentsPerTime];
        for (int i = part.length - 1; i >= 0; i--) {
            part[i] = (byte) (tpart & segmentMask);
            tpart >>= bitsPerSegment;
        }

        long result = 0;
        for (int i = part.length - 1; i >= 0; i--) {
            result <<= bitsPerSegment;
            result |= part[i];
        }

        return result;
    }

    private static long packDate(long segmentMask, int bitsPerSegment, TimestampComponents tscomp) {

        long result = 0;

        result <<= bitsPerSegment;
        result |= tscomp.getDay() & segmentMask;
        result <<= bitsPerSegment;
        result |= tscomp.getMonth() & segmentMask;
        result <<= bitsPerSegment;
        result |= tscomp.getYear() & segmentMask;

        return result;
    }
}