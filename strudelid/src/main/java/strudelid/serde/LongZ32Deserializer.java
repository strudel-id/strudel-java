/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.serde;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import strudelid.layout.LayoutView;

public class LongZ32Deserializer implements Deserializer<Long, String>, ZBase32 {

    // Lambda types
    //@formatter:off
    private static final Function<StringBuilder, StringBuilder>
        withOriginal = (StringBuilder orig) -> orig;
    private static final Function<StringBuilder, StringBuilder>
        withReverseOrder = (StringBuilder orig) -> orig.reverse();
    private static final Function<StringBuilder, StringBuilder>
        withLowerCasing = (StringBuilder orig) -> new StringBuilder(orig.toString().toLowerCase());
    //@formatter:on

    private int bitsPerSegment;
    private String separator;
    private Function<StringBuilder, StringBuilder> ordering;
    private Function<StringBuilder, StringBuilder> casing;
    private Function<String, StringBuilder> stripSeparator;

    @Override
    public void close() {
    }

    @Override
    public void configure(LayoutView layout, FormatView format, int epochYear) {
        bitsPerSegment = layout.getBitsPerSegment();
        ordering = format.isReverse() ? withReverseOrder : withOriginal;
        casing = format.isUpperCase() ? withLowerCasing : withOriginal;
        separator = format.getSeparator();
        stripSeparator = (String orig) -> new StringBuilder(orig.replace(separator, ""));
    }

    @Override
    public Long deserialize(String s) {
        //@formatter:off
        String xs = Stream.of(s)
            .map(stripSeparator)
            .map(casing)
            .map(ordering)
            .collect(Collectors.joining());
        //@formatter:on

        long result = 0;
        for (int i = xs.length() - 1; i >= 0; i--) {
            result <<= bitsPerSegment;
            result |= ZBase32.CHARSET.indexOf(xs.charAt(i));
        }

        return result;
    }
}