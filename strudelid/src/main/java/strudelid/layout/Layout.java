/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid.layout;

public class Layout implements LayoutView {

    private int bitsPerSegment;
    private int maxSegmentValue;
    private long segmentMask;
    private int segmentsPerPartition;
    private int segmentsPerTime;
    private int segmentsPerYear;
    private int segmentsPerMonth;
    private int segmentsPerDay;
    private int segmentsPerDate;
    private int timeResolution;
    private int timePeriodsPerMinute;
    private int timePeriodsPerDay;

    //@formatter:off
    public Layout(
        final int bitsPerSegment,
        final int segmentsPerPartition,
        final int segmentsPerTime,
        final int segmentsPerYear,
        final int segmentsPerMonth,
        final int segmentsPerDay,
        final int timeResolution
    ) {
        setBitsPerSegment(bitsPerSegment);
        setSegmentsPerPartition(segmentsPerPartition);
        setSegmentsPerTime(segmentsPerTime);
        setSegmentsPerYear(segmentsPerYear);
        setSegmentsPerMonth(segmentsPerMonth);
        setSegmentsPerDay(segmentsPerDay);
        setTimeResolution(timeResolution);
    }
    //@formatter:on

    public int getBitsPerSegment() {
        return bitsPerSegment;
    }

    public void setBitsPerSegment(final int bitsPerSegment) {
        updateBitsPerSegment(bitsPerSegment);
    }

    public int getMaxSegmentValue() {
        return maxSegmentValue;
    }

    public long getSegmentMask() {
        return segmentMask;
    }

    public int getSegmentsPerPartition() {
        return segmentsPerPartition;
    }

    public void setSegmentsPerPartition(int segmentsPerPartition) {
        this.segmentsPerPartition = segmentsPerPartition;
    }

    public int getSegmentsPerTime() {
        return segmentsPerTime;
    }

    public void setSegmentsPerTime(int segmentsPerTime) {
        this.segmentsPerTime = segmentsPerTime;
    }

    public int getSegmentsPerYear() {
        return segmentsPerYear;
    }

    public void setSegmentsPerYear(int segmentsPerYear) {
        this.segmentsPerYear = segmentsPerYear;
        updateSegmentsPerDate();
    }

    public int getSegmentsPerMonth() {
        return segmentsPerMonth;
    }

    public void setSegmentsPerMonth(int segmentsPerMonth) {
        this.segmentsPerMonth = segmentsPerMonth;
        updateSegmentsPerDate();
    }

    public int getSegmentsPerDay() {
        return segmentsPerDay;
    }

    public void setSegmentsPerDay(int segmentsPerDay) {
        this.segmentsPerDay = segmentsPerDay;
        updateSegmentsPerDate();
    }

    public int getSegmentsPerDate() {
        return segmentsPerDate;
    }

    public int getTimeResolution() {
        return timeResolution;
    }

    public void setTimeResolution(int timeResolution) {
        updateTimeResolution(timeResolution);
    }

    public int getTimePeriodsPerMinute() {
        return timePeriodsPerMinute;
    }

    public int getTimePeriodsPerDay() {
        return timePeriodsPerDay;
    }

    private void updateBitsPerSegment(final int bitsPerSegment) {
        this.bitsPerSegment = bitsPerSegment;
        maxSegmentValue = 1 << bitsPerSegment;
        segmentMask = maxSegmentValue - 1;
    }

    private void updateSegmentsPerDate() {
        segmentsPerDate = segmentsPerYear + segmentsPerMonth + segmentsPerDay;
    }

    public void updateTimeResolution(int timeResolution) {
        this.timeResolution = timeResolution;
        timePeriodsPerMinute = 60 / timeResolution;
        timePeriodsPerDay = 24 * 60 * timePeriodsPerMinute;
    }
}