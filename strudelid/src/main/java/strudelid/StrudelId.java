/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package strudelid;

import strudelid.layout.LayoutView;
import strudelid.time.Clock;
import strudelid.time.TimestampSequence;

public class StrudelId extends StrudelDatum {

    protected Clock clock;
    protected int timeResolution;

    public static StrudelId getSeed(final LayoutView layout, final Clock clock, final int partition) {
        final int firstTempResolution = layout.getTimeResolution();
        final TimestampSequence firstTimestampSequence = new TimestampSequence(clock.read(), 0);
        return new StrudelId(clock, firstTempResolution, partition, firstTimestampSequence);
    }

    public StrudelId(final Clock clock, final int timeResolution, final int partition,
            final TimestampSequence tssequence) {
        super(partition, tssequence);
        this.clock = clock;
        this.timeResolution = timeResolution;
    }

    public StrudelId next() {
        final TimestampSequence tssequence = getTimestampSequence();

        final long now = clock.read();
        final long fmin_now = now / timeResolution;
        final long fmin_tss = tssequence.getTimestamp() / timeResolution;
        final long next_sequence = fmin_now == fmin_tss ? tssequence.getSequence() + 1 : 0;
        final TimestampSequence next_tssequence = new TimestampSequence(now, next_sequence);

        return new StrudelId(clock, timeResolution, partition, next_tssequence);
    };

    public Clock getClock() {
        return clock;
    }

    public Object getTimeResolution() {
        return timeResolution;
    }

    @Override
    public String toString() {
        return String.format("StrudelId(clock=%s,partition=%d,tssequence=%s", clock.getClass().getSimpleName(),
                partition, tssequence);
    }
}
