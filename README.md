# Strudel ID

Java implementation of Strudel IDs.

## Running Provided Examples

From the [examples](examples) directory:

```shell
# Clean and package
mvn -q clean package

# Run example generator:
mvn -q exec:java@generator

# Run example inspector:
mvn -q exec:java@inspector
```

## Usage

An example of generating Strudel IDs:

```java
package com.example.strudelid;

// Imports elided

public final class ExampleGenerator {

    private ExampleGenerator() {
    }

    public static void main(String[] args) {

        final int count = 10;

        System.out.printf("Generating %d Strudel IDs:\n\n", count);

        final LayoutView layout = LayoutBakery.bakeDefault();
        final FormatView format = FormatBakery.bakeDefault();
        final Clock myClock = new SystemClock();
        final int myPartition = 1;

        StrudelId myGenesis = StrudelId.getSeed(layout, myClock, myPartition);
        Stream<StrudelId> myIds = Stream.iterate(myGenesis, StrudelId::next);

        StrudelIdZ32Serializer formatter = new StrudelIdZ32Serializer();
        formatter.configure(layout, format);
        final Function<StrudelId, String> toZbase32 = formatter::serialize;

        //@formatter:off
        myIds
            .limit(count) // Take the first <count> IDs
            .map(toZbase32) // Serialize them to ZBase-32
            .forEach(System.out::println); // And print them
        //@formatter:off

        System.out.println("\nDone.");
    }
}
```

And an example of inspecting Strudel IDs:

```java
package com.example.strudelid;

// Imports elided

public final class ExampleInspector {

    private final static DateTimeFormatter DATE_FMT = DateTimeFormatter.ofPattern("EEE YYYY-MMM-dd");
    private final static DateTimeFormatter TIME_FMT = DateTimeFormatter.ofPattern("hh:mm:ss a");

    private ExampleInspector() {
    }

    public static void main(String[] args) {

        final String[] myIds = new String[] { "DKW RIJ BG", "DKW FZC B" };

        System.out.println("Inspecting Strudel IDs:");

        final LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        final int thisYear = now.getYear();
        final LayoutView layout = LayoutBakery.bakeDefault();
        final FormatView format = FormatBakery.bakeDefault();

        StrudelIdZ32Deserializer parser = new StrudelIdZ32Deserializer();
        parser.configure(layout, format, thisYear);
        final Function<String, StrudelDatum> parse = parser::deserialize;

        //@formatter:off
        for (int i = 0; i < myIds.length; i++) {
            final String id = myIds[i];
            final StrudelDatum d = parse.apply(id);
            final int p = d.getPartition();
            final long q = d.getTimestampSequence().getSequence();
            final long timestamp = d.getTimestampSequence().getTimestamp();
            LocalDateTime timePeriodStart = LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC);
            LocalDateTime timePeriodEnd = timePeriodStart.plusSeconds(3);
            System.out.printf("\n\"%s\":\n\t- For Partition %d\n\t- On %s\n\t- Was %s ID generated between %s and %s UTC.\n",
                id,
                p,
                timePeriodStart.format(DATE_FMT),
                ordinal(q+1),
                timePeriodStart.format(TIME_FMT),
                timePeriodEnd.format(TIME_FMT)
                );           
        }
        //@formatter:on

        System.out.println("\nDone.");
    }

    public static String ordinal(long i) { /* elided */ }
}

```

## Code Contributions

By contributing code to the Strudel ID project in any form, including sending a
pull request via Gitlab, a code fragment or patch via private email or public
discussion groups, you agree to release your code under the terms of the BSD
license that you can find in the [LICENSE](LICENSE) file included in the Strudel
ID source distribution.
