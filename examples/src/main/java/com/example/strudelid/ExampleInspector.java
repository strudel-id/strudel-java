/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.example.strudelid;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

import strudelid.StrudelDatum;
import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;
import strudelid.serde.FormatBakery;
import strudelid.serde.FormatView;
import strudelid.serde.StrudelIdZ32Deserializer;

public final class ExampleInspector {

  private final static DateTimeFormatter DATE_FMT = DateTimeFormatter.ofPattern("EEE YYYY-MMM-dd");
  private final static DateTimeFormatter TIME_FMT = DateTimeFormatter.ofPattern("hh:mm:ss a");

  private ExampleInspector() {
  }

  public static void main(String[] args) {

    final String[] myIds = new String[] { "DKW RIJ BG", "DKW FZC B" };

    System.out.println("Inspecting Strudel IDs:");

    final LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
    final int thisYear = now.getYear();
    final LayoutView layout = LayoutBakery.bakeDefault();
    final FormatView format = FormatBakery.bakeDefault();

    StrudelIdZ32Deserializer parser = new StrudelIdZ32Deserializer();
    parser.configure(layout, format, thisYear);
    final Function<String, StrudelDatum> parse = parser::deserialize;

    //@formatter:off
        for (int i = 0; i < myIds.length; i++) {
            final String id = myIds[i];
            final StrudelDatum d = parse.apply(id);
            final int p = d.getPartition();
            final long q = d.getTimestampSequence().getSequence();
            final long timestamp = d.getTimestampSequence().getTimestamp();
            LocalDateTime timePeriodStart = LocalDateTime.ofEpochSecond(timestamp, 0, ZoneOffset.UTC);
            LocalDateTime timePeriodEnd = timePeriodStart.plusSeconds(3);
            System.out.printf("\n\"%s\":\n\t- For Partition %d\n\t- On %s\n\t- Was %s ID generated between %s and %s UTC.\n",
                id,
                p,
                timePeriodStart.format(DATE_FMT),
                ordinal(q+1),
                timePeriodStart.format(TIME_FMT),
                timePeriodEnd.format(TIME_FMT)
                );           
        }
        //@formatter:on

    System.out.println("\nDone.");
  }

  public static String ordinal(long i) {
    String[] suffixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
    switch ((int) (i % 100L)) {
      case 11:
      case 12:
      case 13:
        return i + "th";
      default:
        return i + suffixes[(int) (i % 10)];
    }
  }
}
