/*
BSD 3-Clause License

Copyright (c) 2020, Evan P. Mills
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.example.strudelid;

import java.util.stream.Stream;

import strudelid.StrudelId;
import strudelid.layout.LayoutBakery;
import strudelid.layout.LayoutView;
import strudelid.serde.FormatBakery;
import strudelid.serde.FormatView;
import strudelid.serde.StrudelIdZ32Serializer;
import strudelid.time.Clock;
import strudelid.time.SystemClock;

public final class ExampleGenerator {

    private ExampleGenerator() {
    }

    public static void main(String[] args) {

        final int count = 10;

        System.out.printf("Generating %d Strudel IDs:\n\n", count);

        final LayoutView layout = LayoutBakery.bakeDefault();
        final FormatView format = FormatBakery.bakeDefault();
        final Clock myClock = new SystemClock();
        final int myPartition = 1;

        StrudelId myGenesis = StrudelId.getSeed(layout, myClock, myPartition);
        Stream<StrudelId> myIds = Stream.iterate(myGenesis, StrudelId::next);

        StrudelIdZ32Serializer toZ32 = new StrudelIdZ32Serializer();
        toZ32.configure(layout, format);

        //@formatter:off
        myIds
            .limit(count) // Take the first <count> IDs
            .map(toZ32::serialize) // Serialize to ZBase-32
            .forEach(System.out::println); // And print
        //@formatter:off

        System.out.println("\nDone.");
    }
}
